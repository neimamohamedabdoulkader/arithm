module EvalSpec (main, spec) where


import Test.Hspec

import Eval
import Expr

main :: IO ()
main = hspec spec

spec :: Spec
spec =
    describe "eval" $ do
        it "Val 42 " $ eval( Val 42) `shouldBe` 42
       